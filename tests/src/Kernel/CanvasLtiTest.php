<?php

namespace Drupal\Tests\canvas_lti\Kernel;

use Drupal\canvas_lti\CanvasLtiPluginInterface;
use Drupal\canvas_lti\Controller\CanvasLtiToolController;
use Drupal\KernelTests\KernelTestBase;
use Drupal\key\Entity\Key;
use Drupal\key\KeyRepository;
use Firebase\JWT\JWT;
use IMSGlobal\LTI\Cache;
use IMSGlobal\LTI\Cookie;
use IMSGlobal\LTI\LTI_Message_Launch;
use IMSGlobal\LTI\LTI_Registration;
use IMSGlobal\LTI\Redirect;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CanvasLtiTest
 *
 * @package Drupal\Tests\canvas_lti\Kernel
 */
class CanvasLtiTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'key',
    'canvas_lms',
    'canvas_lti',
    'canvas_lti_test'
  ];

  /**
   * @var \Drupal\canvas_lti\CanvasLtiService
   */
  private $service;

  protected function setUp(): void {
    parent::setUp();
    $this->setConfig();
    $this->setMocks();

    $this->service = \Drupal::service('canvas_lti');
    $this->service->setTool('canvas_lti_test');
  }

  /**
   * Tests that the plugin is discovered
   */
  public function testCanvasLtiPlugin() {
    $this->assertArrayHasKey('canvas_lti_test', $this->service->getDefinitions());
  }


  public function testGetRoute() {

    $expectedBase = \Drupal::request()->getSchemeAndHttpHost();

    $route = $this->service->getRoute();
    $expected = $expectedBase . '/canvas_lti/canvas_lti_test';
    $this->assertEquals($expected, $route);

    $route = $this->service->getRoute('login');
    $expected = $expectedBase . '/canvas_lti/login/canvas_lti_test';
    $this->assertEquals($expected, $route);

  }

  public function testSetTool() {
    $this->service->setTool('canvas_lti_test');
    $this->assertInstanceOf(CanvasLtiPluginInterface::class, $this->service->getPlugin());
  }

  public function testRedirect() {
    $_REQUEST = [
      'iss' => 'http://www.example.com',
      'login_hint' => 'test_hint'
    ];

    $redirect = $this->service->redirect();
    $this->assertInstanceOf(Redirect::class, $redirect);
  }

  public function testLaunch() {
    $_POST = [
      'state' => 'test_state',
      'id_token' => $this->getJwt()
    ];
    $this->service->launch();
    $launch = $this->service->getLtiMessageLaunch();
    $this->assertInstanceOf(LTI_Message_Launch::class, $launch);
  }

  public function testTool() {
    $_POST = [
      'state' => 'test_state',
      'id_token' => $this->getJwt()
    ];
    $controller = new CanvasLtiToolController($this->service);
    $response = $controller->tool('canvas_lti_test');
    $this->assertInstanceOf(Response::class,$response);
    $expectedContent = 'Field One is field one';
    $this->assertEquals($expectedContent, $response->getContent());
  }

  private function setConfig() {
    $configFactory = $this->container->get('config.factory');

    $configFactory->getEditable('canvas_lms.settings')
      ->set('institution', 'test_school')
      ->set('environment', 'test')
      ->save();
    $configFactory->getEditable('canvas_lti.settings')
      ->set('lti_key', 'test_key')
      ->save();
    $configFactory->getEditable('canvas_lti_test.settings')
      ->set('canvas_lti', [
        'client_id' => 'test_client_id',
        'deployment_id' => 'test_deployment_id'
      ])
      ->save();
  }

  private function setMocks() {
    $key = $this->createStub(Key::class);
    $key->method('getKeyValue')
      ->willReturn(file_get_contents(__DIR__ . '/private.key'));
    $keyRepository = $this->createStub(KeyRepository::class);
    $keyRepository->method('getKey')
      ->willReturn($key);

    $cookie = $this->createStub(Cookie::class);
    $cookie->method('get_cookie')
      ->with('lti1p3_test_state')
      ->willReturn('test_state');

    $cache = $this->createStub(Cache::class);

    $registration = $this->createStub(LTI_Registration::class);
    $registration->method('get_client_id')
      ->willReturn('test_client_id');
    $registration->method('get_key_set_url')
      ->willReturn(__DIR__ . '/key_set.json');
    $registration->method('get_auth_login_url')
      ->willReturn(__DIR__ . 'https://www.example.com');
    $registration->method($this->anything())
      ->will($this->returnSelf());

    $container = \Drupal::getContainer();
    $container->set('key.repository', $keyRepository);
    $container->set('canvas_lti.cookie', $cookie);
    $container->set('canvas_lti.cache', $cache);
    $container->set('canvas_lti.registration', $registration);
  }

  /**
   * The JWT that would be returned from the LMS when the jwt endpoint is
   * queried for the public key.
   *
   * @return string
   */
  private function getJwt() {
    $message_jwt = [
      "iss" => 'http://www.example.com',
      "aud" => 'test_client_id',
      "sub" => '0ae836b9-7fc9-4060-006f-27b2066ac545',
      "exp" => time() + 600,
      "iat" => time(),
      "nonce" => uniqid("nonce"),
      "https://purl.imsglobal.org/spec/lti/claim/deployment_id" => '8c49a5fa-f955-405e-865f-3d7e959e809f',
      "https://purl.imsglobal.org/spec/lti/claim/message_type" => "LtiResourceLinkRequest",
      "https://purl.imsglobal.org/spec/lti/claim/version" => "1.3.0",
      "https://purl.imsglobal.org/spec/lti/claim/roles" => [
        "http://purl.imsglobal.org/vocab/lis/v2/membership#Instructor"
      ],
      "https://purl.imsglobal.org/spec/lti/claim/resource_link" => [
        "id" => "7b3c5109-b402-4eac-8f61-bdafa301cbb4",
      ],
      "https://purl.imsglobal.org/spec/lti-nrps/claim/namesroleservice" => [
        "context_memberships_url" => "http://lti.localhost/platform/services/nrps",
        "service_versions" => ["2.0"]
      ],
      "https://purl.imsglobal.org/spec/lti-ags/claim/endpoint" => [
        "scope" => [
          "https://purl.imsglobal.org/spec/lti-ags/scope/lineitem",
          "https://purl.imsglobal.org/spec/lti-ags/scope/result.readonly",
          "https://purl.imsglobal.org/spec/lti-ags/scope/score"
        ],
        "lineitems" => "http://lti.localhost/platform/services/ags/lineitems.php",
      ],
      "https://purl.imsglobal.org/spec/lti/claim/custom" => [
        "field_1" => "field one"
      ]
    ];

    return JWT::encode(
      $message_jwt,
      file_get_contents(__DIR__ . '/private.key'),
      'RS256',
      'fcec4f14-28a5-4697-87c3-e9ac361dada5'
    );

  }

}
