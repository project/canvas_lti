<?php

namespace Drupal\canvas_lti_test\Plugin\CanvasLti;

use Drupal\canvas_lti\CanvasLtiPluginBase;

/**
 * Provides an example Canvas Lti plugin.
 *
 * @CanvasLti (
 *   id = "canvas_lti_test",
 *   label = @Translation("Test Plugin"),
 *   description = "@Translation(Test Plugin Description")
 * )
 */
class CanvasLtiTestPlugin extends CanvasLtiPluginBase {

  function build(): array {
    return [
      '#markup' => 'Field One is ' . $this->customFields['field_1']
    ];
  }

}
