<?php

namespace Drupal\canvas_lti;

interface CanvasLtiPluginInterface {

  function build(): array;

  function setCustomFields(array $fields);

}
