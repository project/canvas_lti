<?php

namespace Drupal\canvas_lti;

use Drupal\Component\Plugin\PluginBase;

abstract class CanvasLtiPluginBase extends PluginBase implements CanvasLtiPluginInterface {

  protected $customFields;

  function build(): array {}
  function setCustomFields(array $fields) {
    $this->customFields = $fields;
  }

}
