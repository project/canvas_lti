<?php

namespace Drupal\canvas_lti\Plugin\rest\resource;

use Drupal\canvas_api\CanvasApiServices;
use Drupal\canvas_lti\CanvasLtiDatabase;
use Drupal\canvas_lti\CanvasLtiResourceInterface;
use Drupal\canvas_lti\CanvasLtiService;
use Drupal\canvas_lti\Traits\CanvasLtiTrait;
use Drupal\Core\Database\Connection;
use Drupal\rest\Plugin\ResourceBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


abstract class CanvasLtiResourceBase extends ResourceBase implements CanvasLtiResourceInterface {

  protected $canvasLtiService;
  protected $canvasApiServices;
  protected $ltiDatabase;
  protected $database;
  protected $cache;
  protected $plugin;

  use CanvasLtiTrait;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, $serializer_formats,
                              LoggerInterface $logger,
                              CanvasLtiService $canvasLtiService,
                              CanvasApiServices $canvasApiService,
                              CanvasLtiDatabase $ltiDatabase,
                              Connection $database
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->canvasApiServices = $canvasApiService;
    $this->ltiDatabase = $ltiDatabase;
    $this->canvasLtiService = $canvasLtiService;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('canvas_lti'),
      $container->get('canvas_api'),
      $container->get('canvas_lti.database'),
      $container->get('database')
    );
  }

  protected function setPlugin(){
    $pluginId = $this->getPluginId();
    $this->plugin = $this->canvasLtiPluginManager->createInstance($pluginId);
  }

}