<?php

namespace Drupal\canvas_lti\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Canvas LTI annotation object.
 *
 * @Annotation
 */
class CanvasLti extends Plugin {

  /**
   * The id of the Canvas LTI.
   *
   * @var string
   */
  public $id;

  /**
   * The title of the Canvas LTI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * A description of the Canvas LTI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
