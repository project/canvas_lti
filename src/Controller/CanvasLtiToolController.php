<?php

namespace Drupal\canvas_lti\Controller;

use Drupal\canvas_lti\CanvasLtiService;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use IMSGlobal\LTI\LTI_Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CanvasLtiToolController implements ContainerInjectionInterface {

  protected $lti;

  const CUSTOM_FIELDS = 'https://purl.imsglobal.org/spec/lti/claim/custom';

  private $canvasLtiPluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(CanvasLtiService $canvasLtiService) {
    $this->lti = $canvasLtiService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('canvas_lti'),
    );
  }

  public function tool($tool): Response {

    // Check to see if the requested tool plugin exists
    try {
      $this->lti->setTool($tool);
    } catch (PluginException $e) {
      return new Response($e->getMessage());
    }

    try {
      $this->lti->launch();
    } catch (LTI_Exception $e) {
      return new Response($e->getMessage());
    }

    return $this->lti->render();

  }

  protected function setPlugin(){
    $pluginId = $this->getPluginId();
    $this->plugin = $this->canvasLtiPluginManager->createInstance($pluginId);
  }


}
