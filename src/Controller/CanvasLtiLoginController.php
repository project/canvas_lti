<?php

namespace Drupal\canvas_lti\Controller;

use Drupal\canvas_lti\CanvasLtiService;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use IMSGlobal\LTI\OIDC_Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for handling the Client's (Canvas') initial authorization request
 */
class CanvasLtiLoginController implements ContainerInjectionInterface {

  /**
   * @var \Drupal\canvas_lti\CanvasLtiDatabase
   */
  protected $ltiDatabase;

  /**
   * An array of discovered Canvas Lti plugins
   *
   * @var array
   */
  protected $definitions;

  /**
   * @var \Drupal\canvas_lti\CanvasLtiPluginManager
   */
  protected $canvasLtiPluginManager;

  /**
   * @var \Symfony\Component\HttpFoundation\ParameterBag
   */
  private $lti;

  public function __construct(CanvasLtiService $canvasLtiService) {
    $this->lti = $canvasLtiService;
  }

  public static function create(ContainerInterface $container): CanvasLtiLoginController {
    return new static(
      $container->get('canvas_lti')
    );
  }

  /**
   * Builds the authorization grant for the requested tool and redirects back
   * to Canvas
   *
   * @param $tool
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function login($tool): Response {

    // Check to see if the requested tool plugin exists
    try {
      $this->lti->setTool($tool);
    } catch (PluginException $e) {
      return new Response($e->getMessage());
    }

    try {
      $redirect = $this->lti->redirect();
      $redirect->do_redirect();
    } catch (OIDC_Exception $e) {
      return new Response($e->getMessage());
    }

  }

}
