<?php

namespace Drupal\canvas_lti\Controller;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\key\KeyRepository;
use IMSGlobal\LTI\JWKS_Endpoint;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class CanvasLtiTokenController implements ContainerInjectionInterface {

  /**
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * @var \Drupal\key\KeyRepository
   */
  private $keyRepository;

  public function __construct(ConfigFactory $configFactory,
                              KeyRepository $keyRepository
  ) {
    $this->config = $configFactory->get('canvas_lti.settings');
    $this->keyRepository = $keyRepository;
  }

  /*
  * {@inheritdoc}
  */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('key.repository')
    );
  }

  public function jwks(){
    $keyRepositoryKey = $this->config->get('lti_key');
    $key = $this->keyRepository->getKey($keyRepositoryKey);
    $jwt = JWKS_Endpoint::new([
      $key->uuid() => $key->getKeyValue()
    ])->get_public_jwks();
    return new JsonResponse($jwt);
  }

}
