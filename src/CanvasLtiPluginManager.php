<?php

namespace Drupal\canvas_lti;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\canvas_lti\Annotation\CanvasLti;
use Traversable;

/**
 * A plugin manager for Canvas LTI plugins.
 */
class CanvasLtiPluginManager extends DefaultPluginManager {

  /**
   * Creates the discovery object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $subdir = 'Plugin/CanvasLti';
    $plugin_interface = CanvasLtiPluginInterface::class;
    $plugin_definition_annotation_name = CanvasLti::class;
    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);
    $this->alterInfo('canvas_lti_info');
    $this->setCacheBackend($cache_backend, 'canvas_lti_info');
  }

}
