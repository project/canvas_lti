<?php

namespace Drupal\canvas_lti;

use Drupal\canvas_lti\Lti\CanvasLtiMessageLaunch;
use Drupal\canvas_lti\Lti\CanvasLtiOidcLogin;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Url;
use IMSGlobal\LTI\LTI_Message_Launch;
use Symfony\Component\HttpFoundation\Response;
use IMSGlobal\LTI\Redirect;

class CanvasLtiService {

  private $plugin;

  private $ltiMessageLaunch;

  private $definitions;


  public function __construct(
    CanvasLtiPluginManager $pluginManager,
    CanvasLtiMessageLaunch $ltiMessageLaunch,
    CanvasLtiOidcLogin $oidcLogin,
    Renderer $renderer
  ) {
    $this->pluginManager = $pluginManager;
    $this->ltiMessageLaunch = $ltiMessageLaunch;
    $this->oidcLogin = $oidcLogin;
    $this->renderer = $renderer;
  }

  public function getDefinitions() {
    return $this->pluginManager->getDefinitions();
  }

  public function getRoute($login = '') {
    $routeName = 'canvas_lti.' . ($login ?: 'tool');
    return Url::fromRoute($routeName)
      ->setAbsolute()
      ->setRouteParameter('tool', $this->plugin->getPluginId())
      ->toString();
  }

  public function setTool($tool) {
    $pluginDefinition = $this->pluginManager->getDefinition($tool);
    $this->plugin = $this->pluginManager->createInstance($pluginDefinition['id']);
    $this->definitions = $this->plugin->getPluginDefinition();
  }

  /**
   * @throws \IMSGlobal\LTI\LTI_Exception
   */
  public function launch(): void {
    $this->ltiMessageLaunch = $this->ltiMessageLaunch
      ->getInstance($this->definitions['provider'])
      ->validate();
  }

  /**
   * Returns a redirect to the tool requester.
   *
   * @throws \IMSGlobal\LTI\OIDC_Exception
   */
  public function redirect(): Redirect {
    return $this->oidcLogin
      ->getInstance($this->definitions['provider'])
      ->do_oidc_login_redirect($this->getRoute());
  }

  /**
   * @return \Drupal\canvas_lti\CanvasLtiPluginInterface
   */
  public function getPlugin() {
    return $this->plugin;
  }

  /**
   * @return \IMSGlobal\LTI\LTI_Message_Launch
   */
  public function getLtiMessageLaunch(): LTI_Message_Launch {
    return $this->ltiMessageLaunch;
  }


  public function render(): Response {
    $this->setCustomFields();
    $build = $this->plugin->build();
    $html = $this->renderer->renderRoot($build);
    return new Response($html);
  }

  private function setCustomFields() {
    $data = $this->ltiMessageLaunch->get_launch_data() ?: [];
    $this->plugin->setCustomFields($data[CanvasLtiMessageLaunch::CUSTOM_FIELDS]);
  }
}
