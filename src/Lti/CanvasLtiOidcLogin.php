<?php

namespace Drupal\canvas_lti\Lti;

use IMSGlobal\LTI\Cache;
use IMSGlobal\LTI\Cookie;
use IMSGlobal\LTI\LTI_OIDC_Login;

class CanvasLtiOidcLogin {

  public function __construct(
    CanvasLtiDatabase $ltiDatabase,
    Cache $ltiCache,
    Cookie $ltiCookie
  ) {
    $this->ltiDatabase = $ltiDatabase;
    $this->ltiCache = $ltiCache;
    $this->ltiCookie = $ltiCookie;
  }

  public function getInstance($provider) {
    $this->ltiDatabase->setProvider($provider);
    return LTI_OIDC_Login::new($this->ltiDatabase, $this->ltiCache, $this->ltiCookie);
  }
}
