<?php

namespace Drupal\canvas_lti\Lti;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\key\KeyRepositoryInterface;
use IMSGlobal\LTI;
use IMSGlobal\LTI\LTI_Registration;

class CanvasLtiDatabase implements LTI\Database {

  private $keyRepository;
  private $config;

  /**
   * @var \IMSGlobal\LTI\LTI_Registration
   */
  private $ltiRegistration;

  private $provider;

  public function __construct(KeyRepositoryInterface $keyRepository,
                              ConfigFactoryInterface $config,
                              LTI_Registration $ltiRegistration
  ) {
    $this->keyRepository = $keyRepository;
    $this->config = $config;
    $this->ltiRegistration = $ltiRegistration;
  }

  public function setProvider($provider) {
    $this->provider = $provider;
  }

  public function find_registration_by_issuer($iss): LTI\LTI_Registration {

    $canvasUrl = $this->getCanvasUrl();
    $institution = $this->config->get('canvas_lms.settings')->get('institution');
    $institutionUrl = str_replace('canvas', $institution, $canvasUrl);

    $providerConfig = $this->config->get($this->provider . '.settings')->get('canvas_lti');
    // @TODO: Modules should be able to define multiple tools
    // $toolConfig = $providerConfig->get($tool['id']);
    $toolConfig = $providerConfig;

    return $this->ltiRegistration
      ->set_auth_login_url($canvasUrl . '/api/lti/authorize_redirect')
      ->set_auth_token_url($institutionUrl . '/login/oauth2/token')
      ->set_client_id($toolConfig['client_id'])
      ->set_key_set_url($canvasUrl . '/api/lti/security/jwks')
      ->set_issuer($iss)
      ->set_tool_private_key($this->getPrivateKey());
  }

  public function find_deployment($iss, $deployment_id) {
    return LTI\LTI_Deployment::new()
      ->set_deployment_id($deployment_id);
  }

  private function getPrivateKey(): string {
    $keyRepositoryKey = $this->config->get('canvas_lti.settings')->get('lti_key');
    return $this->keyRepository->getKey($keyRepositoryKey)->getKeyValue();
  }

  private function getCanvasUrl(): string {
    $environment = $this->config->get('canvas_lms.settings')->get('environment');
    return 'https://canvas.' . ($environment == 'production' ? '' : $environment . '.') . 'instructure.com';
  }




}
