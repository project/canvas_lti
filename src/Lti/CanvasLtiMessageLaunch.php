<?php

namespace Drupal\canvas_lti\Lti;

use IMSGlobal\LTI\Cache;
use IMSGlobal\LTI\Cookie;
use IMSGlobal\LTI\LTI_Message_Launch;


class CanvasLtiMessageLaunch extends LTI_Message_Launch {

  const CUSTOM_FIELDS = 'https://purl.imsglobal.org/spec/lti/claim/custom';

  /**
   * @var \Drupal\canvas_lti\Lti\CanvasLtiDatabase
   */
  private $ltiDatabase;

  /**
   * @var \IMSGlobal\LTI\Cache
   */
  private $ltiCache;

  /**
   * @var \IMSGlobal\LTI\Cookie
   */
  private $ltiCookie;

  public function __construct(
    CanvasLtiDatabase $ltiDatabase,
    Cache $ltiCache,
    Cookie $ltiCookie
  ) {
    $this->ltiDatabase = $ltiDatabase;
    $this->ltiCache = $ltiCache;
    $this->ltiCookie = $ltiCookie;
  }

  public function getInstance($provider) {
    $this->ltiDatabase->setProvider($provider);
    return LTI_Message_Launch::new($this->ltiDatabase, $this->ltiCache, $this->ltiCookie);
  }

}




