<?php

namespace Drupal\canvas_lti\Lti;

use IMSGlobal\LTI\Cookie;

class CanvasLtiCookie extends Cookie {

  public function set_cookie($name, $value, $exp = 3600, $options = []) {
    $options = array_merge($options, ['path' => '/']);
    parent::set_cookie($name, $value, $exp, $options);
    return $this;
  }
}
