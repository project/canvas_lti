<?php

namespace Drupal\canvas_lti;

interface CanvasLtiResourceInterface {

  /**
   * Return the plugin ID this resource wishes to use
   *
   * @return string
   *   The plugin ID
   */
  public function getPluginId();

}
