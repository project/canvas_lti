<?php

namespace Drupal\canvas_lti\Form;

use Drupal\canvas_lti\CanvasLtiPluginManager;
use Drupal\Component\Uuid\Uuid;
use Drupal\Core\Field\Plugin\Field\FieldType\UuidItem;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
/**
 * Defines a form to configure the settings for the Canvas integration.
 */
class CanvasLtiSettingsForm extends ConfigFormBase {

  protected $definitions;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\canvas_lti\CanvasLtiPluginManager $canvasPluginManager
   */
  public function __construct(ConfigFactoryInterface $config_factory,
                              CanvasLtiPluginManager $canvasPluginManager
  ) {
    parent::__construct($config_factory);
    $this->definitions = $canvasPluginManager->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.canvas_lti')
    );
  }
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'canvas_lti_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    $configs = array_map(function ($value) {return $value . '.settings';} , array_column($this->definitions, 'provider'));
    $configs[] = 'canvas_lti.settings';
    return $configs;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {

    $config = $this->config('canvas_lti.settings');

    $form['#tree'] = TRUE;

    $form['lti_key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('LTI Key'),
      '#default_value' => $config->get('lti_key'),
    ];

    $form['providers'] = [
      '#tree' => TRUE
    ];

    foreach($this->definitions as $definition){

      $provider = $definition['provider'];
      $defaults = $this->config($provider . '.settings')->get('canvas_lti');

      $form['providers'][$provider] = [
        '#type' => 'details',
        '#title' => $definition['label'],
      ];

      $form['providers'][$provider]['client_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Client ID'),
        '#default_value' => $defaults['client_id'],
      ];

      $form['providers'][$provider]['deployment_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Deployment ID'),
        '#default_value' => $defaults['deployment_id'],
      ];

    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $form_state->cleanValues();
    $values = $form_state->getValues();

    $this->config('canvas_lti.settings')
      ->set('lti_key', $values['lti_key'])
      ->set('api_key', $values['api_key'])
      ->save();
    foreach($values['providers'] as $key => $value){
        $this->config($key . '.settings')
          ->set('canvas_lti', $value)
          ->save();
    }

    parent::submitForm($form, $form_state);

  }
}
